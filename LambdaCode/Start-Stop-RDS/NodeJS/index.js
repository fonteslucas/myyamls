let AWS = require('aws-sdk');

let rds = new AWS.RDS();

function startStopRdsDB(action) {

    return new Promise(async (resolve, reject) => {

        let instanceList = [];
        let startedInstanceCount = 0;
        let stoppedInstanceCount = 0;

        try {
            console.log("=== Get RDS instance list.");
            instanceList = await getRdsInstanceList();
        } catch (err) {
            reject(err);
        }

        for (let instance of instanceList) {

            try {
                if (['mysql', 'mariadb', 'postgres'].indexOf(instance.Engine) > -1 || instance.Engine.includes('oracle')) {

					console.log("Current status of DB instance " + instance.DBInstanceIdentifier + " is " + instance.DBInstanceStatus);

					if (action == 'start' && instance.DBInstanceStatus == 'stopped') {

						console.log("DB instance " + instance.DBInstanceIdentifier + " that can be started.");
						await startRdsInstance(instance.DBInstanceIdentifier);
						console.log("DB instance " + instance.DBInstanceIdentifier + " is started.");
						startedInstanceCount++;
					}

					if (action == 'stop' && instance.DBInstanceStatus == 'available') {

						console.log("DB instance " + instance.DBInstanceIdentifier + " that can be stopped.");
						await stopRdsInstance(instance.DBInstanceIdentifier);
						console.log("DB instance " + instance.DBInstanceIdentifier + " is stopped.");
						stoppedInstanceCount++;
					}
                }
            } catch (err) {

                reject(err);
            }
        }

        if (startedInstanceCount == 0 && action == 'start') console.log("Found 0 RDS instance that can be started.");
        if (stoppedInstanceCount == 0 && action == 'stop') console.log("Found 0 RDS instance that can be stopped.");

        resolve("Method startStopRdsDB() is done.");

    });
}

function getRdsInstanceList() {

    return new Promise((resolve, reject) => {

        let params = null;

        rds.describeDBInstances(params, function (err, data) {

            if (err) {
                reject(err);
            } else {
                resolve(data.DBInstances);
            }
        });
    });
}

function startRdsInstance(dbInstanceIdentifier) {

    return new Promise((resolve, reject) => {

        let params = {
            DBInstanceIdentifier: dbInstanceIdentifier
        };

        rds.startDBInstance(params, function (err) {

            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function stopRdsInstance(dbInstanceIdentifier) {

    return new Promise((resolve, reject) => {

        let params = {
            DBInstanceIdentifier: dbInstanceIdentifier
        };

        rds.stopDBInstance(params, function (err) {

            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

exports.handler = async (event, context, callback) => {

    try {
        const action = event.action.toLowerCase();
        console.log("=== action:", action);

        if (action && ['start', 'stop'].indexOf(action) > -1) {

            let resolveOfStartStopRdsDB = await startStopRdsDB(action);
            console.log(resolveOfStartStopRdsDB);

        } else {
            console.log("Action was neither start nor stop. Lambda function aborted.");
        }

        console.log("=== Complete")
        return callback(null, "Complete");

    } catch (err) {
        return callback(err, null);
    }

};